package com.example.jazminnerae.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class TempConverter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_converter);
    }

    public void add(View v)
    {
        TextView result=(TextView)findViewById(R.id.txtOutput);
        EditText temp=(EditText)findViewById(R.id.tbTemperature);


        double a=Double.parseDouble(String.valueOf(temp.getText()));
        RadioButton radCelsius=(RadioButton)findViewById(R.id.radFahtoCel);
        RadioButton radFahrenheit=(RadioButton)findViewById(R.id.radCeltoFah);


        if(radCelsius.isChecked())
        {

            result.setText(fahtocel(a)+" degree C");
            radCelsius.setChecked(false);
            radFahrenheit.setChecked(true);
        }
        if(radFahrenheit.isChecked()) {
            result.setText(celtofah(a) + " degree F");
            radFahrenheit.setChecked(false);
            radCelsius.setChecked(true);
        }
    }

    private double celtofah(double c)
    {
        return (c*9)/5+32;
    }

    private double fahtocel(double f)
    {
        return (f-32)*5/9;
    }
}

